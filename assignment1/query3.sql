-- incomprehensible release, ratings and phones

EXPLAIN ANALYZE
SELECT f1.release_year,
       max(f2.rating),
       (SELECT max(phone) FROM address) AS max_phone
FROM film f1,
     film f2
WHERE f1.length > 120
  AND f1.rental_duration > f2.rental_duration
GROUP BY f1.release_year;

CREATE INDEX idx_film__rental_duration ON film USING btree(rental_duration);
DROP INDEX IF EXISTS idx_film__rental_duration;

CREATE INDEX idx_address__phone ON address USING btree(phone);
DROP INDEX IF EXISTS idx_address__phone;

CREATE INDEX idx_address__phone ON address using spgist(phone);
DROP INDEX IF EXISTS idx_address__phone;

-- without index
/*
HashAggregate  (cost=7747.19..7747.20 rows=1 width=40) (actual time=132.261..132.263 rows=1 loops=1)
  Group Key: f1.release_year
  Batches: 1  Memory Usage: 24kB
  InitPlan 1 (returns $0)
    ->  Aggregate  (cost=15.54..15.55 rows=1 width=32) (actual time=0.124..0.125 rows=1 loops=1)
          ->  Seq Scan on address  (cost=0.00..14.03 rows=603 width=12) (actual time=0.008..0.053 rows=603 loops=1)
  ->  Nested Loop  (cost=0.00..6971.64 rows=152000 width=8) (actual time=0.633..96.173 rows=190138 loops=1)
        Join Filter: (f1.rental_duration > f2.rental_duration)
        Rows Removed by Join Filter: 266862
        ->  Seq Scan on film f2  (cost=0.00..64.00 rows=1000 width=6) (actual time=0.010..0.500 rows=1000 loops=1)
        ->  Materialize  (cost=0.00..68.78 rows=456 width=6) (actual time=0.000..0.029 rows=457 loops=1000)
              ->  Seq Scan on film f1  (cost=0.00..66.50 rows=456 width=6) (actual time=0.008..0.374 rows=457 loops=1)
                    Filter: (length > 120)
                    Rows Removed by Filter: 543
Planning Time: 0.236 ms
Execution Time: 132.314 ms
*/

-- with index
/*
HashAggregate  (cost=5310.27..5310.28 rows=1 width=40) (actual time=135.123..135.124 rows=1 loops=1)
  Group Key: f1.release_year
  Batches: 1  Memory Usage: 24kB
  InitPlan 1 (returns $0)
    ->  Aggregate  (cost=15.54..15.55 rows=1 width=32) (actual time=0.124..0.124 rows=1 loops=1)
          ->  Seq Scan on address  (cost=0.00..14.03 rows=603 width=12) (actual time=0.007..0.053 rows=603 loops=1)
  ->  Nested Loop  (cost=0.15..4534.72 rows=152000 width=8) (actual time=0.039..93.681 rows=190138 loops=1)
        ->  Seq Scan on film f1  (cost=0.00..66.50 rows=456 width=6) (actual time=0.017..0.452 rows=457 loops=1)
              Filter: (length > 120)
              Rows Removed by Filter: 543
        ->  Index Scan using idx_film__rental_duration on film f2  (cost=0.15..6.47 rows=333 width=6) (actual time=0.003..0.108 rows=416 loops=457)
              Index Cond: (rental_duration < f1.rental_duration)
Planning Time: 0.248 ms
Execution Time: 135.170 ms
*/