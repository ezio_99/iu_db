-- auditing staff

EXPLAIN ANALYZE
SELECT r1.staff_id, p1.payment_date
FROM rental r1,
     payment p1
WHERE r1.rental_id = p1.rental_id
  AND NOT EXISTS(SELECT 1
                 FROM rental r2,
                      customer c
                 WHERE r2.customer_id =
                       c.customer_id
                   AND active = 1
                   AND r2.last_update > r1.last_update);

CREATE INDEX idx_rental__last_update ON rental USING btree(last_update);
DROP INDEX IF EXISTS idx_rental__last_update;

CREATE INDEX idx_customer__active ON customer USING hash(active);
DROP INDEX IF EXISTS idx_customer__active;

-- without index
/*
Nested Loop Anti Join  (cost=534.78..2284549.49 rows=9731 width=10) (actual time=7288.891..21372.736 rows=1 loops=1)
  Join Filter: (r2.last_update > r1.last_update)
  Rows Removed by Join Filter: 200448775
  ->  Hash Join  (cost=510.99..803.28 rows=14596 width=18) (actual time=427.905..449.953 rows=14596 loops=1)
        Hash Cond: (p1.rental_id = r1.rental_id)
        ->  Seq Scan on payment p1  (cost=0.00..253.96 rows=14596 width=12) (actual time=0.021..6.773 rows=14596 loops=1)
        ->  Hash  (cost=310.44..310.44 rows=16044 width=14) (actual time=427.785..427.787 rows=16044 loops=1)
              Buckets: 16384  Batches: 1  Memory Usage: 881kB
              ->  Seq Scan on rental r1  (cost=0.00..310.44 rows=16044 width=14) (actual time=423.637..425.252 rows=16044 loops=1)
  ->  Materialize  (cost=23.79..454.85 rows=15642 width=8) (actual time=0.000..0.705 rows=13734 loops=14596)
        ->  Hash Join  (cost=23.79..376.64 rows=15642 width=8) (actual time=0.209..4.620 rows=15640 loops=1)
              Hash Cond: (r2.customer_id = c.customer_id)
              ->  Seq Scan on rental r2  (cost=0.00..310.44 rows=16044 width=10) (actual time=0.006..1.193 rows=16044 loops=1)
              ->  Hash  (cost=16.49..16.49 rows=584 width=4) (actual time=0.191..0.192 rows=584 loops=1)
                    Buckets: 1024  Batches: 1  Memory Usage: 29kB
                    ->  Seq Scan on customer c  (cost=0.00..16.49 rows=584 width=4) (actual time=0.006..0.087 rows=584 loops=1)
                          Filter: (active = 1)
                          Rows Removed by Filter: 15
Planning Time: 0.351 ms
JIT:
  Functions: 26
"  Options: Inlining true, Optimization true, Expressions true, Deforming true"
"  Timing: Generation 6.581 ms, Inlining 13.620 ms, Optimization 272.790 ms, Emission 136.990 ms, Total 429.981 ms"
Execution Time: 21379.755 ms
*/

-- with index
/*
Nested Loop Anti Join  (cost=535.06..1557011.13 rows=9731 width=10)
  ->  Hash Join  (cost=510.99..803.28 rows=14596 width=18)
        Hash Cond: (p1.rental_id = r1.rental_id)
        ->  Seq Scan on payment p1  (cost=0.00..253.96 rows=14596 width=12)
        ->  Hash  (cost=310.44..310.44 rows=16044 width=14)
              ->  Seq Scan on rental r1  (cost=0.00..310.44 rows=16044 width=14)
  ->  Hash Join  (cost=24.07..131.84 rows=5214 width=8)
        Hash Cond: (r2.customer_id = c.customer_id)
        ->  Index Scan using idx_rental__last_update on rental r2  (cost=0.29..93.92 rows=5348 width=10)
              Index Cond: (last_update > r1.last_update)
        ->  Hash  (cost=16.49..16.49 rows=584 width=4)
              ->  Seq Scan on customer c  (cost=0.00..16.49 rows=584 width=4)
                    Filter: (active = 1)
JIT:
  Functions: 26
"  Options: Inlining true, Optimization true, Expressions true, Deforming true"
*/
