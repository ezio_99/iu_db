-- number of payments that are smaller
-- EXPLAIN ANALYZE
EXPLAIN
SELECT *,
       (SELECT count(*)
        FROM rental r2,
             payment p2
        WHERE r2.rental_id = p2.rental_id
          AND p2.amount < p.amount) AS
           count_smaller_pay
FROM rental r,
     payment p
WHERE r.rental_id =
      p.rental_id;

CREATE INDEX idx1 ON payment USING btree(rental_id);
DROP INDEX idx1;

CREATE INDEX idx_payment__amount__rental_id ON payment USING btree(amount, rental_id);
DROP INDEX IF EXISTS idx_payment__amount__rental_id;

-- without index
/*
Hash Join  (cost=510.99..12062691.37 rows=14596 width=70) (actual time=409.889..52158.080 rows=14596 loops=1)
  Hash Cond: (p.rental_id = r.rental_id)
  ->  Seq Scan on payment p  (cost=0.00..253.96 rows=14596 width=26) (actual time=0.012..2.927 rows=14596 loops=1)
  ->  Hash  (cost=310.44..310.44 rows=16044 width=36) (actual time=398.676..398.676 rows=16044 loops=1)
        Buckets: 16384  Batches: 1  Memory Usage: 1379kB
        ->  Seq Scan on rental r  (cost=0.00..310.44 rows=16044 width=36) (actual time=0.003..1.056 rows=16044 loops=1)
  SubPlan 1
    ->  Aggregate  (cost=826.37..826.38 rows=1 width=8) (actual time=3.543..3.543 rows=1 loops=14596)
          ->  Hash Join  (cost=510.99..814.21 rows=4865 width=0) (actual time=0.282..3.206 rows=6134 loops=14596)
                Hash Cond: (p2.rental_id = r2.rental_id)
                ->  Seq Scan on payment p2  (cost=0.00..290.45 rows=4865 width=4) (actual time=0.280..1.924 rows=6134 loops=14596)
                      Filter: (amount < p.amount)
                      Rows Removed by Filter: 8462
                ->  Hash  (cost=310.44..310.44 rows=16044 width=4) (actual time=4.271..4.272 rows=16044 loops=1)
                      Buckets: 16384  Batches: 1  Memory Usage: 693kB
                      ->  Seq Scan on rental r2  (cost=0.00..310.44 rows=16044 width=4) (actual time=0.008..1.507 rows=16044 loops=1)
Planning Time: 0.251 ms
JIT:
  Functions: 25
"  Options: Inlining true, Optimization true, Expressions true, Deforming true"
"  Timing: Generation 6.026 ms, Inlining 25.500 ms, Optimization 237.956 ms, Emission 131.596 ms, Total 401.078 ms"
Execution Time: 52167.189 ms
*/

-- with index
/*
Hash Join  (cost=510.99..10237789.98 rows=14596 width=70) (actual time=349.747..36029.533 rows=14596 loops=1)
  Hash Cond: (p.rental_id = r.rental_id)
  ->  Seq Scan on payment p  (cost=0.00..253.96 rows=14596 width=26) (actual time=0.014..6.706 rows=14596 loops=1)
  ->  Hash  (cost=310.44..310.44 rows=16044 width=36) (actual time=339.655..339.656 rows=16044 loops=1)
        Buckets: 16384  Batches: 1  Memory Usage: 1379kB
        ->  Seq Scan on rental r  (cost=0.00..310.44 rows=16044 width=36) (actual time=0.004..1.064 rows=16044 loops=1)
  SubPlan 1
    ->  Aggregate  (cost=701.35..701.36 rows=1 width=8) (actual time=2.442..2.442 rows=1 loops=14596)
          ->  Hash Join  (cost=511.28..689.18 rows=4865 width=0) (actual time=0.014..2.104 rows=6134 loops=14596)
                Hash Cond: (p2.rental_id = r2.rental_id)
                ->  Index Only Scan using idx_payment__amount__rental_id on payment p2  (cost=0.29..165.42 rows=4865 width=4) (actual time=0.013..0.878 rows=6134 loops=14596)
                      Index Cond: (amount < p.amount)
                      Heap Fetches: 0
                ->  Hash  (cost=310.44..310.44 rows=16044 width=4) (actual time=4.172..4.172 rows=16044 loops=1)
                      Buckets: 16384  Batches: 1  Memory Usage: 693kB
                      ->  Seq Scan on rental r2  (cost=0.00..310.44 rows=16044 width=4) (actual time=0.006..1.486 rows=16044 loops=1)
Planning Time: 0.487 ms
JIT:
  Functions: 23
"  Options: Inlining true, Optimization true, Expressions true, Deforming true"
"  Timing: Generation 7.224 ms, Inlining 26.729 ms, Optimization 211.981 ms, Emission 97.400 ms, Total 343.334 ms"
Execution Time: 36039.636 ms
*/


