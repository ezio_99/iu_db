-- Abuzyar Tazetdinov
-- Danila Moriakov
-- Zakhar Yagudin

-- https://dbdiagram.io/d/5fa98a513a78976d7b7b2a3f

CREATE DATABASE MOVIE_DB;

-- connect to DB and run commands below

CREATE TABLE "company" (
  "id" int PRIMARY KEY,
  "name" varchar(200) NOT NULL,
  "address" varchar(200) NOT NULL
);

CREATE TABLE "person" (
  "id" int PRIMARY KEY,
  "first_name" varchar(100) NOT NULL,
  "last_name" varchar(100) NOT NULL,
  "birth_date" timestamp NOT NULL
);

CREATE TABLE "role" (
  "id" int PRIMARY KEY,
  "name" varchar(100)
);

CREATE TABLE "quote" (
  "id" int PRIMARY KEY,
  "quote" text NOT NULL
);

CREATE TABLE "genre" (
  "id" int PRIMARY KEY,
  "name" varchar(200)
);

CREATE TABLE "movie" (
  "id" int PRIMARY KEY,
  "title" varchar(200) NOT NULL,
  "year_of_release" timestamp NOT NULL,
  "length" int,
  "company_id" int
);

CREATE TABLE "movie_person" (
  "movie_id" int PRIMARY KEY,
  "person_id" int,
  "role_id" int
);

CREATE TABLE "genre_movie" (
  "movie_id" int,
  "genre_id" int,
  PRIMARY KEY ("movie_id", "genre_id")
);

CREATE TABLE "quote_person_movie" (
  "movie_id" int,
  "person_id" int,
  "quote_id" int,
  PRIMARY KEY ("movie_id", "person_id", "quote_id")
);

ALTER TABLE "movie" ADD FOREIGN KEY ("company_id") REFERENCES "company" ("id");

ALTER TABLE "movie_person" ADD FOREIGN KEY ("movie_id") REFERENCES "movie" ("id");

ALTER TABLE "movie_person" ADD FOREIGN KEY ("person_id") REFERENCES "person" ("id");

ALTER TABLE "movie_person" ADD FOREIGN KEY ("role_id") REFERENCES "role" ("id");

ALTER TABLE "genre_movie" ADD FOREIGN KEY ("movie_id") REFERENCES "movie" ("id");

ALTER TABLE "genre_movie" ADD FOREIGN KEY ("genre_id") REFERENCES "genre" ("id");

ALTER TABLE "quote_person_movie" ADD FOREIGN KEY ("movie_id") REFERENCES "movie" ("id");

ALTER TABLE "quote_person_movie" ADD FOREIGN KEY ("person_id") REFERENCES "person" ("id");

ALTER TABLE "quote_person_movie" ADD FOREIGN KEY ("quote_id") REFERENCES "quote" ("id");
