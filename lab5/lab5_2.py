import psycopg2

from config import db_credentials
from lab5_1 import execute_queries

queries = [
    "EXPLAIN ANALYZE SELECT * FROM customer",

    "EXPLAIN ANALYZE SELECT * FROM customer WHERE review @@ to_tsquery('successful')"  # gin or gist index => speed up
]


def drop_indexes(cursor):
    cursor.execute("DROP INDEX IF EXISTS customer__review__gin__index")
    cursor.execute("DROP INDEX IF EXISTS customer__review__gist__index")


def print_results(before: dict, gin: dict, gist: dict):
    def print_tuple(t: tuple):
        for i in t:
            print(f"      {i[0]}")

    for k in before:
        print(f"  {k}\n"
              "    Before:")
        print_tuple(before[k])

        print("    GIN:")
        print_tuple(gin[k])

        print("    GiST:")
        print_tuple(gist[k])


def main():
    connection = psycopg2.connect(**db_credentials)
    print(f"Connected to database {db_credentials['database']}")

    cursor = connection.cursor()

    try:
        drop_indexes(cursor)
        print("Indexes dropped")

        executed_before_creating_indexes = execute_queries(cursor, queries)
        print("Queries executed")

        cursor.execute("CREATE INDEX customer__review__gin__index ON customer USING gin(review)")
        print("GIN index created")

        executed_after_creating_gin_index = execute_queries(cursor, queries)
        print("Queries executed")

        drop_indexes(cursor)
        print("Indexes dropped")

        cursor.execute("CREATE INDEX customer__review__gist__index ON customer USING gist(review)")
        print("GiST index created")

        executed_after_creating_gist_index = execute_queries(cursor, queries)
        print("Queries executed", end="\n\n")

        print("Results:")
        print_results(executed_before_creating_indexes,
                      executed_after_creating_gin_index,
                      executed_after_creating_gist_index)
        print()
    finally:
        cursor.close()
        connection.close()
        print(f"Disconnected from database {db_credentials['database']}")


if __name__ == "__main__":
    main()
