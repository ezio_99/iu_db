import psycopg2

from config import db_credentials

queries = [
    "EXPLAIN ANALYZE SELECT * FROM customer",

    "EXPLAIN ANALYZE SELECT * FROM customer WHERE age = 21",  # btree index => speed up
    "EXPLAIN ANALYZE SELECT * FROM customer WHERE age >= 18 AND age <= 30",  # btree index => speed up

    "EXPLAIN ANALYZE SELECT * FROM customer WHERE name = 'Jason Bernard'",  # hash index => speed up
    "EXPLAIN ANALYZE SELECT * FROM customer WHERE name > 'J'",  # hash index => no checking to equality => no speed up
]


def drop_indexes(cursor):
    cursor.execute("DROP INDEX IF EXISTS customer__age__index")
    cursor.execute("DROP INDEX IF EXISTS customer__name__index")


def create_indexes(cursor):
    cursor.execute("CREATE INDEX customer__age__index ON customer USING btree(age)")
    cursor.execute("CREATE INDEX customer__name__index ON customer USING hash(name)")


def execute_explain_analyze_query(cursor, query: str) -> str:
    assert query.upper().startswith("EXPLAIN ANALYZE")
    cursor.execute(query)
    result = cursor.fetchall()
    return result


def execute_queries(cursor, queries: list) -> dict:
    data = {}
    for query in queries:
        result = execute_explain_analyze_query(cursor, query)
        data[query] = result
    return data


def print_results(before: dict, after: dict):
    def print_tuple(t: tuple):
        for i in t:
            print(f"      {i[0]}")

    for k in before:
        print(f"  {k}\n"
              "    Before:")
        print_tuple(before[k])

        print("    After:")
        print_tuple(after[k])


def main():
    connection = psycopg2.connect(**db_credentials)
    print(f"Connected to database {db_credentials['database']}")

    cursor = connection.cursor()

    try:
        drop_indexes(cursor)
        print("Indexes dropped")

        executed_before_creating_indexes = execute_queries(cursor, queries)
        print("Queries executed")

        create_indexes(cursor)
        print("Indexes created")

        executed_after_creating_indexes = execute_queries(cursor, queries)
        print("Queries executed", end="\n\n")

        print("Results:")
        print_results(executed_before_creating_indexes, executed_after_creating_indexes)
        print()
    finally:
        cursor.close()
        connection.close()
        print(f"Disconnected from database {db_credentials['database']}")


if __name__ == "__main__":
    main()
