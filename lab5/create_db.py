import psycopg2
from faker import Faker
from psycopg2 import sql
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT
from psycopg2.extras import execute_values

from config import default_db_credentials, db_credentials


def create_tables(cursor):
    cursor.execute("""
            CREATE TABLE IF NOT EXISTS customer (
                id      SERIAL      PRIMARY KEY,
                name    TEXT        NOT NULL,
                address TEXT        NOT NULL,
                age     INT         NOT NULL,
                review  TSVECTOR    NOT NULL
            )
        """)


def insert_data(connection, cursor):
    fake = Faker()
    records_per_insert = 1000
    number_of_inserts = 100
    insert_query = "INSERT INTO customer(name, address, age, review) VALUES %s"

    for insert_count in range(1, number_of_inserts + 1):
        print(f"Inserted {insert_count * records_per_insert} records to customer")

        data = []
        for _ in range(records_per_insert):
            data.append((fake.name(), fake.address(), fake.random.randint(18, 80), fake.text()))

        execute_values(cursor, insert_query, data, page_size=1000)

    connection.commit()

    return records_per_insert * number_of_inserts


def main():
    # Drop and create new database
    connection = psycopg2.connect(**default_db_credentials)
    print(f"Connected to database {default_db_credentials['database']}")

    connection.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)

    cursor = connection.cursor()

    try:
        cursor.execute(sql.SQL("DROP DATABASE IF EXISTS {}").format(sql.Identifier(db_credentials["database"])))
        print(f"Database {db_credentials['database']} dropped successfully")

        cursor.execute(sql.SQL("CREATE DATABASE {}").format(sql.Identifier(db_credentials["database"])))
        print(f"Database {db_credentials['database']} created successfully")
    finally:
        cursor.close()
        connection.close()
        print(f"Disconnected from database {default_db_credentials['database']}")

    # Create table and insert data
    connection = psycopg2.connect(**db_credentials)
    print(f"Connected to database {db_credentials['database']}")

    cursor = connection.cursor()

    try:
        create_tables(cursor)
        print("Tables created successfully")

        inserted_count = insert_data(connection, cursor)
        print(f"Added {inserted_count} records")
    finally:
        cursor.close()
        connection.close()
        print(f"Disconnected from database {db_credentials['database']}")


if __name__ == "__main__":
    main()
