# Info
You can run python scripts manually or using `run` script (usage: `./run path_to_your_python3_interpreter`).

Log of `run`'s execution saved in `run.log` file.

`create_db.py` will remove database `lab5_Abuzyar_Tazetdinov` and create new database, create table and insert data.

`lab5_1.py` - Exercise 1.

`lab5_2.py` - Exercise 2.
