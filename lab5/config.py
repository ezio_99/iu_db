from copy import deepcopy


default_db_credentials = {
    "database": "postgres",
    "user": "postgres",
    "password": "postgres",
    "host": "127.0.0.1",
    "port": "5432",
}

db_credentials = deepcopy(default_db_credentials)
db_credentials["database"] = "lab5_Abuzyar_Tazetdinov"
