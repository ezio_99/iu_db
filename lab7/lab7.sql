-- Exercise 2

CREATE OR REPLACE FUNCTION get_customers(start int, end_ int)
RETURNS SETOF customer AS
$$
BEGIN
    IF start < 0 THEN
        RAISE EXCEPTION 'start should be positive integer, given %', start;
    END IF;

    IF end_ < 0 THEN
        RAISE EXCEPTION 'end should be positive integer, given %', end_;
    END IF;

    IF start > end_ THEN
        RAISE EXCEPTION 'end should be greater or equal to start, given start = %, end = %', start, end_;
    END IF;

    IF end_ > 600 THEN
        RAISE EXCEPTION 'end should be lower or equal to 600, given %', end_;
    END IF;

    RETURN QUERY
        SELECT *
        FROM customer
        ORDER BY address_id
        OFFSET start - 1
        LIMIT end_ - start + 1;
END;
$$
LANGUAGE plpgsql;

SELECT * FROM get_customers(1, 10);

SELECT * FROM get_customers(1, 600);

SELECT * FROM get_customers(1, 601);

SELECT * FROM get_customers(10, 1);

SELECT * FROM get_customers(-1, 2);

SELECT * FROM get_customers(-1, -2);
