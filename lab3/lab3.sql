-- Order countries by id asc, then show the 12th to 17th rows
SELECT * FROM country ORDER BY country_id OFFSET 11 LIMIT 6;

-- List all addresses in a city whose name starts with 'A'
SELECT a.* FROM city
    LEFT JOIN address a on city.city_id = a.city_id
WHERE upper(city.city) LIKE 'A%';

-- List all customers' first name, last name and the city they live in
SELECT first_name, last_name, c.city FROM customer
    LEFT JOIN address a on customer.address_id = a.address_id
    LEFT JOIN city c on a.city_id = c.city_id;

-- Find all customers with at least one payment whose amount is greater than 11 dollars
SELECT customer.* FROM customer
    INNER JOIN payment p on customer.customer_id = p.customer_id
WHERE p.amount > 11;

-- Find all duplicated first names in the customer table
SELECT subquery.first_name FROM (
    SELECT first_name, count(first_name) AS counter FROM customer
           GROUP BY first_name
) AS subquery
WHERE subquery.counter > 1;

-- Create 2 Views of your choice

DROP VIEW IF EXISTS cities_starting_from_a;

-- First view: get all city's names starting from A
CREATE VIEW cities_starting_from_a AS
SELECT city.city FROM city
WHERE upper(city.city) LIKE 'A%';

DROP VIEW IF EXISTS duplicated_customer_first_names;

-- Second view: get all duplicated customer's first name
CREATE VIEW duplicated_customer_first_names AS
SELECT subquery.first_name FROM (
    SELECT first_name, count(first_name) AS counter FROM customer
           GROUP BY first_name
) AS subquery
WHERE subquery.counter > 1;

-- Fetch all duplicated customer's first name
SELECT * FROM duplicated_customer_first_names;

-- Function for updating last_update field
CREATE OR REPLACE FUNCTION update_last_update_field()
  RETURNS TRIGGER
  LANGUAGE PLPGSQL
  AS
$$
BEGIN
	NEW.last_update = now();

	RETURN NEW;
END;
$$;

DROP TRIGGER IF EXISTS update_city_last_update_field_trigger ON city;

-- Trigger for updating last_update field in city table
CREATE TRIGGER update_city_last_update_field_trigger
    BEFORE UPDATE ON city
    FOR EACH ROW
    EXECUTE PROCEDURE update_last_update_field();

-- Testing trigger
DELETE FROM city WHERE city.city = 'Kazan';

-- country_id = 4 for Angola
INSERT INTO city(city, country_id) VALUES ('Kazan', 4);

SELECT * FROM city WHERE city.city = 'Kazan'; -- uuups, we inserted wrong country_id -> going to update

-- country_id = 80 for Russian Federation
UPDATE city
SET country_id = 80
WHERE city = 'Kazan';

-- checking last_update
SELECT * FROM city WHERE city.city = 'Kazan'; -- last_update field is updated also -> trigger works
