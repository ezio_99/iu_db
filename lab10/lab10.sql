DROP TABLE IF EXISTS account;

CREATE TABLE account
(
    id       serial PRIMARY KEY,
    username varchar(100),
    fullname varchar(200),
    balance  int,
    group_id int
);


INSERT INTO account(username, fullname, balance, group_id)
VALUES ('jones', 'Alice Jones', 82, 1),
       ('bitdiddl', 'Ben Bitdiddle', 65, 1),
       ('mike', 'Michael Dole', 73, 2),
       ('alyssa', 'Alyssa P. Hacker', 79, 3),
       ('bbrown', 'Bob Brown', 100, 3);


SELECT *
FROM account