CREATE
(kn:Fighter {name: "Khabib", surname: "Nurmagomedov", weight: 155}),
(rda:Fighter {name: "Rafael", surname: "Dos Anjos", weight: 155}),
(nm:Fighter {name: "Neil", surname: "Magny", weight: 170}),
(jj:Fighter {name: "Jon", surname: "Jones", weight: 205}),
(dc:Fighter {name: "Daniel", surname: "Cormier", weight: 205}),
(mb:Fighter {name: "Michael", surname: "Bisping", weight: 185}),
(mh:Fighter {name: "Matt", surname: "Hamill", weight: 185}),
(bv:Fighter {name: "Brandon", surname: "Vera", weight: 205}),
(fm:Fighter {name: "Frank", surname: "Mir", weight: 230}),
(bl:Fighter {name: "Brock", surname: "Lesnar", weight: 230}),
(kg:Fighter {name: "Kelvin", surname: "Gastelum", weight: 185}),

(kn)-[:beats]->(rda),
(rda)-[:beats]->(nm),
(jj)-[:beats]->(dc),
(mb)-[:beats]->(mh),
(jj)-[:beats]->(bv),
(bv)-[:beats]->(fm),
(fm)-[:beats]->(bl),
(nm)-[:beats]->(kg),
(kg)-[:beats]->(mb),
(mb)-[:beats]->(mh),
(mb)-[:beats]->(kg),
(mh)-[:beats]->(jj);


// queries

// Return all middle/Walter/light weight fighters (155,170,185) who at least have one win.
MATCH (a:Fighter)-[:beats]->(b)
WITH a, b,
COUNT(b) AS cnt
WHERE a.weight in [155, 170, 185] AND
cnt > 0
RETURN a, cnt;

// Return fighters who had 1-1 record with each other. Use Count from the aggregation functions.
MATCH (a:Fighter)-[:beats]->(b)-[:beats]->(a)
RETURN a;

// Return all fighter that “Khabib Nurmagomedov” can beat them and he didn’t have a fight with them yet.
MATCH
(a:Fighter({name: "Khabib", surname: "Nurmagomedov"})) -[:beats]-> () -[:beats*0..]-> (b)
RETURN b;

// Return undefeated Fighters(0 loss), defeated fighter (0 wins).

// Return all fighters MMA records and create query to enter the record as a property for a fighter {name, weight, record}.
