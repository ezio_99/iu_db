-- Exercise 1, part 1

CREATE OR REPLACE FUNCTION send_money(from_ int, to_ int, amount int)
    RETURNS void AS
$$
BEGIN
    UPDATE account SET credit = (SELECT credit FROM account WHERE id = from_) - amount WHERE id = from_;
    UPDATE account SET credit = (SELECT credit FROM account WHERE id = to_) + amount WHERE id = to_;
END;
$$
    LANGUAGE plpgsql;

DROP TABLE IF EXISTS ledger;
DROP TABLE IF EXISTS account;

CREATE TABLE account
(
    id     serial primary key,
    name   varchar(100),
    credit int,
    CONSTRAINT positive_credit CHECK (credit >= 0)
);

INSERT INTO account(id, name, credit)
VALUES (1, 'Ilsur', 1000),
       (2, 'Danila', 1000),
       (3, 'Zakhar', 1000);

SELECT *
FROM account;


START TRANSACTION;

SELECT 1
FROM send_money(1, 3, 500);
SELECT 1
FROM send_money(2, 1, 700);
SELECT 1
FROM send_money(2, 3, 100);

SELECT *
FROM account;

ROLLBACK;

SELECT *
FROM account;


-- Exercise 1, part 2

ALTER TABLE account
    ADD COLUMN bank_name varchar(100);

UPDATE account
SET bank_name = 'SpearBank'
WHERE id IN (1, 3);

UPDATE account
SET bank_name = 'Tinkoff'
WHERE id = 2;

INSERT INTO account(id, name, credit)
VALUES (4, 'Fees', 0);

CREATE OR REPLACE FUNCTION send_money(from_ int, to_ int, amount int)
    RETURNS void AS
$$
BEGIN
    IF (SELECT bank_name FROM account WHERE id = from_) <> (SELECT bank_name FROM account WHERE id = to_) THEN
        UPDATE account SET credit = (SELECT credit FROM account WHERE id = from_) - amount - 30 WHERE id = from_;
        UPDATE account SET credit = (SELECT credit FROM account WHERE id = 4) + 30 WHERE id = 4;
    ELSE
        UPDATE account SET credit = (SELECT credit FROM account WHERE id = from_) - amount WHERE id = from_;
    END IF;

    UPDATE account SET credit = (SELECT credit FROM account WHERE id = to_) + amount WHERE id = to_;
END;
$$
    LANGUAGE plpgsql;


START TRANSACTION;

SELECT 1
FROM send_money(1, 3, 500);
SELECT 1
FROM send_money(2, 1, 700);
SELECT 1
FROM send_money(2, 3, 100);

SELECT *
FROM account;

ROLLBACK;

SELECT *
FROM account;


-- Exercise 2

DROP TABLE IF EXISTS ledger;

CREATE TABLE ledger
(
    id                    serial primary key,
    from_                 int,
    to_                   int,
    fee                   int,
    amount                int,
    transaction_date_time timestamp,

    FOREIGN KEY (from_) REFERENCES account (id),
    FOREIGN KEY (to_) REFERENCES account (id),

    CONSTRAINT not_same_sender CHECK (from_ <> to_)
);

CREATE OR REPLACE FUNCTION send_money(from_ int, to_ int, amount int)
    RETURNS void AS
$$
BEGIN
    IF (SELECT bank_name FROM account WHERE id = from_) <> (SELECT bank_name FROM account WHERE id = to_) THEN
        UPDATE account SET credit = (SELECT credit FROM account WHERE id = from_) - amount - 30 WHERE id = from_;
        UPDATE account SET credit = (SELECT credit FROM account WHERE id = 4) + 30 WHERE id = 4;
        INSERT INTO ledger(from_, to_, fee, amount, transaction_date_time)
        VALUES (from_, to_, 30, amount, now());
    ELSE
        UPDATE account SET credit = (SELECT credit FROM account WHERE id = from_) - amount WHERE id = from_;
        INSERT INTO ledger(from_, to_, fee, amount, transaction_date_time)
        VALUES (from_, to_, 0, amount, now());
    END IF;

    UPDATE account SET credit = (SELECT credit FROM account WHERE id = to_) + amount WHERE id = to_;
END;
$$
    LANGUAGE plpgsql;

START TRANSACTION;

SELECT 1
FROM send_money(1, 3, 500);
SELECT 1
FROM send_money(2, 1, 700);
SELECT 1
FROM send_money(2, 3, 100);

SELECT *
FROM account;

SELECT *
FROM ledger;

ROLLBACK;

SELECT *
FROM account;

SELECT *
FROM ledger;
