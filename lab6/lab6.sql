-- Query 1

EXPLAIN SELECT first_name, last_name, title
FROM customer c, film f
LEFT JOIN film_category fc ON fc.film_id = f.film_id
LEFT JOIN category c2 on fc.category_id = c2.category_id
WHERE NOT EXISTS(
        SELECT i.film_id, customer_id
        FROM rental
        LEFT JOIN inventory i ON rental.inventory_id = i.inventory_id
        WHERE i.film_id = f.film_id AND c.customer_id = rental.customer_id
    )
AND (f.rating = 'R' OR f.rating = 'PG-13')
AND (c2.name = 'Horror' OR c2.name = 'Sci-Fi');

/*
Hash Anti Join  (cost=722.87..3211.03 rows=1179 width=28)
  Hash Cond: ((c.customer_id = rental.customer_id) AND (f.film_id = i.film_id))
  ->  Nested Loop  (cost=1.54..454.29 rows=28078 width=36)
        ->  Seq Scan on customer c  (cost=0.00..14.99 rows=599 width=17)
        ->  Materialize  (cost=1.54..87.50 rows=47 width=19)
              ->  Nested Loop  (cost=1.54..87.27 rows=47 width=19)
                    ->  Hash Join  (cost=1.26..20.58 rows=125 width=2)
                          Hash Cond: (fc.category_id = c2.category_id)
                          ->  Seq Scan on film_category fc  (cost=0.00..16.00 rows=1000 width=4)
                          ->  Hash  (cost=1.24..1.24 rows=2 width=4)
                                ->  Seq Scan on category c2  (cost=0.00..1.24 rows=2 width=4)
                                      Filter: (((name)::text = 'Horror'::text) OR ((name)::text = 'Sci-Fi'::text))
                    ->  Index Scan using film_pkey on film f  (cost=0.28..0.53 rows=1 width=19)
                          Index Cond: (film_id = fc.film_id)
                          Filter: ((rating = 'R'::mpaa_rating) OR (rating = 'PG-13'::mpaa_rating))
  ->  Hash  (cost=480.67..480.67 rows=16044 width=4)
        ->  Hash Join  (cost=128.07..480.67 rows=16044 width=4)
              Hash Cond: (rental.inventory_id = i.inventory_id)
              ->  Seq Scan on rental  (cost=0.00..310.44 rows=16044 width=6)
              ->  Hash  (cost=70.81..70.81 rows=4581 width=6)
                    ->  Seq Scan on inventory i  (cost=0.00..70.81 rows=4581 width=6)
*/

-- Optimization for Query 1

EXPLAIN SELECT first_name, last_name, title
FROM customer, film f
LEFT JOIN film_category fc ON fc.film_id = f.film_id
LEFT JOIN category c on fc.category_id = c.category_id
WHERE (f.film_id, customer_id) NOT IN (
        SELECT i.film_id, customer_id
        FROM rental
        LEFT JOIN inventory i ON rental.inventory_id = i.inventory_id
    )
AND (f.rating = 'R' OR f.rating = 'PG-13')
AND (c.name = 'Horror' OR c.name = 'Sci-Fi');

/*
Nested Loop  (cost=522.32..1115.84 rows=14039 width=28)
  Join Filter: (NOT (hashed SubPlan 1))
  ->  Seq Scan on customer  (cost=0.00..14.99 rows=599 width=17)
  ->  Materialize  (cost=1.54..87.50 rows=47 width=19)
        ->  Nested Loop  (cost=1.54..87.27 rows=47 width=19)
              ->  Hash Join  (cost=1.26..20.58 rows=125 width=2)
                    Hash Cond: (fc.category_id = c.category_id)
                    ->  Seq Scan on film_category fc  (cost=0.00..16.00 rows=1000 width=4)
                    ->  Hash  (cost=1.24..1.24 rows=2 width=4)
                          ->  Seq Scan on category c  (cost=0.00..1.24 rows=2 width=4)
                                Filter: (((name)::text = 'Horror'::text) OR ((name)::text = 'Sci-Fi'::text))
              ->  Index Scan using film_pkey on film f  (cost=0.28..0.53 rows=1 width=19)
                    Index Cond: (film_id = fc.film_id)
                    Filter: ((rating = 'R'::mpaa_rating) OR (rating = 'PG-13'::mpaa_rating))
  SubPlan 1
    ->  Hash Left Join  (cost=128.07..480.67 rows=16044 width=4)
          Hash Cond: (rental.inventory_id = i.inventory_id)
          ->  Seq Scan on rental  (cost=0.00..310.44 rows=16044 width=6)
          ->  Hash  (cost=70.81..70.81 rows=4581 width=6)
                ->  Seq Scan on inventory i  (cost=0.00..70.81 rows=4581 width=6)
*/

-- Query 2

EXPLAIN SELECT a.address_id, max(total_payment) AS max
FROM (
    SELECT address.address_id, sum(payment.amount) AS total_payment
    FROM payment
    LEFT JOIN customer ON customer.customer_id = payment.customer_id
    LEFT JOIN address ON customer.address_id = address.address_id
    LEFT JOIN city ON address.city_id = city.city_id
    WHERE date_trunc('month', payment_date) IN (SELECT date_trunc('month', max(payment_date)) FROM payment)
    GROUP BY city.city, address.address_id
) AS a
GROUP BY a.address_id
ORDER BY max DESC;

/*
Sort  (cost=650.34..650.53 rows=73 width=36)
  Sort Key: (max(a.total_payment)) DESC
  ->  GroupAggregate  (cost=646.81..648.09 rows=73 width=36)
        Group Key: a.address_id
        ->  Sort  (cost=646.81..646.99 rows=73 width=36)
              Sort Key: a.address_id
              ->  Subquery Scan on a  (cost=642.18..644.55 rows=73 width=36)
                    ->  GroupAggregate  (cost=642.18..643.82 rows=73 width=45)
"                          Group Key: city.city, address.address_id"
                          ->  Sort  (cost=642.18..642.36 rows=73 width=19)
"                                Sort Key: city.city, address.address_id"
                                ->  Hash Right Join  (cost=625.94..639.92 rows=73 width=19)
                                      Hash Cond: (city.city_id = address.city_id)
                                      ->  Seq Scan on city  (cost=0.00..11.00 rows=600 width=13)
                                      ->  Hash  (cost=625.02..625.02 rows=73 width=12)
                                            ->  Hash Right Join  (cost=608.00..625.02 rows=73 width=12)
                                                  Hash Cond: (address.address_id = customer.address_id)
                                                  ->  Seq Scan on address  (cost=0.00..14.03 rows=603 width=6)
                                                  ->  Hash  (cost=607.09..607.09 rows=73 width=8)
                                                        ->  Nested Loop Left Join  (cost=290.76..607.09 rows=73 width=8)
                                                              ->  Hash Join  (cost=290.48..585.48 rows=73 width=8)
"                                                                    Hash Cond: (date_trunc('month'::text, payment.payment_date) = (date_trunc('month'::text, max(payment_1.payment_date))))"
                                                                    ->  Seq Scan on payment  (cost=0.00..253.96 rows=14596 width=16)
                                                                    ->  Hash  (cost=290.47..290.47 rows=1 width=8)
                                                                          ->  Aggregate  (cost=290.45..290.46 rows=1 width=8)
                                                                                ->  Seq Scan on payment payment_1  (cost=0.00..253.96 rows=14596 width=8)
                                                              ->  Index Scan using customer_pkey on customer  (cost=0.28..0.30 rows=1 width=6)
*/

-- Optimization for Query 2

CREATE INDEX payment__payment_date__index ON payment USING btree(payment_date);

EXPLAIN SELECT a.address_id, max(total_payment) AS max
FROM (
    SELECT address.address_id, sum(payment.amount) AS total_payment
    FROM payment
    LEFT JOIN customer ON customer.customer_id = payment.customer_id
    LEFT JOIN address ON customer.address_id = address.address_id
    LEFT JOIN city ON address.city_id = city.city_id
    WHERE date_trunc('month', payment_date) IN (SELECT date_trunc('month', max(payment_date)) FROM payment)
    GROUP BY city.city, address.address_id
) AS a
GROUP BY a.address_id
ORDER BY max DESC;

/*
Sort  (cost=360.21..360.39 rows=73 width=36)
  Sort Key: (max(a.total_payment)) DESC
  ->  GroupAggregate  (cost=356.67..357.95 rows=73 width=36)
        Group Key: a.address_id
        ->  Sort  (cost=356.67..356.85 rows=73 width=36)
              Sort Key: a.address_id
              ->  Subquery Scan on a  (cost=352.04..354.41 rows=73 width=36)
                    ->  GroupAggregate  (cost=352.04..353.68 rows=73 width=45)
"                          Group Key: city.city, address.address_id"
                          ->  Sort  (cost=352.04..352.22 rows=73 width=19)
"                                Sort Key: city.city, address.address_id"
                                ->  Hash Right Join  (cost=335.80..349.78 rows=73 width=19)
                                      Hash Cond: (city.city_id = address.city_id)
                                      ->  Seq Scan on city  (cost=0.00..11.00 rows=600 width=13)
                                      ->  Hash  (cost=334.89..334.89 rows=73 width=12)
                                            ->  Hash Right Join  (cost=317.87..334.89 rows=73 width=12)
                                                  Hash Cond: (address.address_id = customer.address_id)
                                                  ->  Seq Scan on address  (cost=0.00..14.03 rows=603 width=6)
                                                  ->  Hash  (cost=316.95..316.95 rows=73 width=8)
                                                        ->  Nested Loop Left Join  (cost=0.62..316.95 rows=73 width=8)
                                                              ->  Hash Join  (cost=0.35..295.34 rows=73 width=8)
"                                                                    Hash Cond: (date_trunc('month'::text, payment.payment_date) = (date_trunc('month'::text, $0)))"
                                                                    ->  Seq Scan on payment  (cost=0.00..253.96 rows=14596 width=16)
                                                                    ->  Hash  (cost=0.34..0.34 rows=1 width=8)
                                                                          ->  Result  (cost=0.31..0.33 rows=1 width=8)
                                                                                InitPlan 1 (returns $0)
                                                                                  ->  Limit  (cost=0.29..0.31 rows=1 width=8)
                                                                                        ->  Index Only Scan Backward using payment__payment_date__index on payment payment_1  (cost=0.29..423.72 rows=14596 width=8)
                                                                                              Index Cond: (payment_date IS NOT NULL)
                                                              ->  Index Scan using customer_pkey on customer  (cost=0.28..0.30 rows=1 width=6)
*/
