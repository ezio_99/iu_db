// Find all the documents in the collection restaurants
db.restaurants.find({}, {_id: 0});

// Find the fields restaurant_id, name, borough and cuisine for all the documents in the collection restaurant.
db.restaurants.find({}, {
    restaurant_id: 1,
    name: 1,
    borough: 1,
    cuisine: 1,
    _id: 0
});

// Find the first 5 restaurant which is in the borough Bronx.
db.restaurants.find({}, {_id: 0})
    .sort({restaurant_id: 1})
    .limit(5);

// Find the restaurant Id, name, borough and cuisine
// for those restaurants which prepared dish except 'American' and 'Chinees'
// or restaurant's name begins with letter 'Wil’.
db.restaurants.find({
    $or: [
        {cuisine: {$not: {$eq: 'American'}}},
        {cuisine: {$not: {$eq: 'Chinees'}}},
        {name: /^Wil/}
    ]
}, {
    restaurant_id: 1,
    name: 1,
    borough: 1,
    cuisine: 1,
    _id: 0
});

// Find the restaurant name, borough, longitude and attitude and cuisine
// for those restaurants which contains 'mon' as three letters somewhere in its name.
db.restaurants.aggregate([
    {
        $match: {name: /.*[Mm]on.*/}
    },
    {
        $project: {
            name: 1,
            borough: 1,
            longitude: {$first: {$slice: ["$address.coord", 1, 1]}},
            attitude: {$first: {$slice: ["$address.coord", 0, 1]}},
            cuisine: 1,
            _id: 0,
        }
    }
]);

// Find the restaurant Id, name, borough and cuisine
// for those restaurants which belong to the borough
// Staten Island or Queens or Bronx or Brooklyn.
db.restaurants.find({
    $or: [
        {borough: 'Staten Island'},
        {borough: 'Queens'},
        {borough: 'Bronx'},
        {borough: 'Brooklyn'}
    ]
}, {
    restaurant_id: 1,
    name: 1,
    borough: 1,
    cuisine: 1,
    _id: 0
});
